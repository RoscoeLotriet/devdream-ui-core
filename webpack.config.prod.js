var path = require('path');
var webpack = require('webpack');

module.exports = {
  devtool: 'source-map',
  entry: [
    './lib/main'
  ],
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'bundle.js',
    publicPath: '/static/'
  },
  plugins: [
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify('production')
      }
    })
  ],
  resolve: {
    extensions: ['', '.js', '.jsx']
  },
  module: {
    loaders: [{
      test: /\.js[x*]?$/,
      loaders: ['babel-loader'],
      include: path.join(__dirname, 'lib')
    },
    {
      test: /\.css$/,
      loaders: ['style-loader', 'css-loader']
    },
    {
      test: /\.json$/,
      loader: 'json-loader'
    },
    {
      test: /\.less$/,
      loaders: ['style-loader', 'css-loader', 'less-loader'],
    },
    {
      test: /\.woff\d?(\?.+)?$/,
      loader: 'url-loader?limit=10000&minetype=application/font-woff',
    },
    {
      test: /\.ttf(\?.+)?$/,
      loader: 'url-loader?limit=10000&minetype=application/octet-stream',
    },
    {
      test: /\.eot(\?.+)?$/,
      loader: 'file-loader',
    },
    {
      test: /\.svg(\?.+)?$/,
      loader: 'url-loader?limit=10000&minetype=image/svg+xml',
    },
    {
      test: /\.png$/,
      loader: 'url-loader?limit=10000&mimetype=image/png',
    },
    {
      test: require.resolve('devdream-material-design-lite'),
      loader: 'exports?componentHandler'
    }]
  },
  node: {
    net: 'empty',
    tls: 'empty'
  }
};
