var path = require('path');
var webpack = require('webpack');

module.exports = {
  devtool: 'eval',
    entry: [
    'webpack-hot-middleware/client?reload=true',
    './ui-test/ui-test'
  ],
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'bundle.js',
    publicPath: '/static/'
  },
  plugins: [
  new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin()    
  ],
  module: {
    loaders: [{
      test: /\.js$/,
      loader: 'babel',
      include: [path.join(__dirname, 'ui-test')],
      query: {
                stage: 0,
                plugins: ['react-transform'],
                extra: {
                "react-transform": {
                  "transforms": [{
                    "transform": "react-transform-hmr",
                    "imports": ["react"],
                    "locals": ["module"]
                }, {
                    "transform": "react-transform-catch-errors",
                    "imports": ["react", "redbox-react"]
                }]
              }
                }
      }
    },
    {
      test: /\.css$/,
      loaders: ['style-loader', 'css-loader']
    },
    {
      test: /\.json$/,
      loader: 'json-loader'
    },
    {
      test: /\.less$/,
      loaders: ['style-loader', 'css-loader', 'less-loader'],
    },
    {
      test: /\.woff\d?(\?.+)?$/,
      loader: 'url-loader?limit=10000&minetype=application/font-woff',
    },
    {
      test: /\.ttf(\?.+)?$/,
      loader: 'url-loader?limit=10000&minetype=application/octet-stream',
    },
    {
      test: /\.eot(\?.+)?$/,
      loader: 'file-loader',
    },
    {
      test: /\.svg(\?.+)?$/,
      loader: 'url-loader?limit=10000&minetype=image/svg+xml',
    },
    {
      test: /\.png$/,
      loader: 'url-loader?limit=10000&mimetype=image/png',
    },
    {
      test: require.resolve('devdream-material-design-lite'),
      loader: 'exports?componentHandler'
    }]
  },
  node: {
    net: 'empty',
    tls: 'empty'
  }
};
