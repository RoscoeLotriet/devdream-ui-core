import LayoutDrawer from '../lib/LayoutDrawer'
import createComponent from './util/create-component'

describe('LayoutDrawer component', () => {

	let layoutDrawer;

	beforeEach(() => {
		layoutDrawer = createComponent(LayoutDrawer, {})
	});

	it('should apply class "mdl-layout__drawer"', () => {
		expect(layoutDrawer.props.className).toMatch('mdl-layout__drawer');
	})

	it('should render child components', ()=>{

		layoutDrawer = createComponent(LayoutDrawer, {}, 'Child')
		expect(layoutDrawer.props.children).toMatch('Child');

	})

})