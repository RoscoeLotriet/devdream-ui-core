import Grid from '../lib/Grid'
import createComponent from './util/create-component'

describe('Grid component', () => {

	let grid;

	beforeEach(() => {
		grid = createComponent(Grid, {})
		
	});

	it('should apply class "mdl-grid"', ()=>{
		expect(grid.props.className).toMatch('mdl-grid');
	})

	it('should apply class "mdl-grid--no-spacing" when noSpacing == true', ()=>{

		grid = createComponent(Grid, {noSpacing: true})
		expect(grid.props.className).toMatch('mdl-grid--no-spacing');
	})

})