import LayoutIcon from '../lib/LayoutIcon'
import createComponent from './util/create-component'

describe('LayoutIcon component', () => {

	let layoutIcon;

	beforeEach(() => {
		layoutIcon = createComponent(LayoutIcon, {})
	});

	it('should apply class "mdl-layout-icon"', () => {
		expect(layoutIcon.props.className).toMatch('mdl-layout-icon');
	})

})