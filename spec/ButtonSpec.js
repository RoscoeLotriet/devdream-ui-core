import Button from '../lib/Button'
import createComponent from './util/create-component'
import createMDLComponentInstance from './util/create-mdl-component-instance'

describe('Button component', () => {

	let button;

	beforeEach(() => {
		button = createMDLComponentInstance(Button, {})
		
	});

	it('should apply classes "mdl-button mdl-js-button"', ()=>{
		expect(button.props.className).toMatch('mdl-button mdl-js-button');
	})

	it('should apply class "mdl-button--raised" when raised == true', ()=>{
		button = createMDLComponentInstance(Button, {raised: true})
		expect(button.props.className).toMatch('mdl-button--raised');
	})

	it('should apply class "mdl-button--fab" when fab == true', ()=>{
		button = createMDLComponentInstance(Button,{fab: true})
		expect(button.props.className).toMatch('mdl-button--fab');
	})

	it('should apply class "mdl-button--mini-fab" when miniFab == true', ()=>{
		button = createMDLComponentInstance(Button,{miniFab: true})
		expect(button.props.className).toMatch('mdl-button--mini-fab');
	})

	it('should apply class "mdl-button--icon" when icon == true', ()=>{
		button = createMDLComponentInstance(Button,{icon: true})
		expect(button.props.className).toMatch('mdl-button--icon');
	})

	it('should apply class "mdl-button--colored" when colored == true', ()=>{
		button = createMDLComponentInstance(Button,{colored: true})
		expect(button.props.className).toMatch('mdl-button--colored');
	})

	it('should apply class "mdl-button--primary" when primary == true', ()=>{
		button = createMDLComponentInstance(Button,{primary: true})
		expect(button.props.className).toMatch('mdl-button--primary');
	})

	it('should apply class "mdl-button--accent" when accent == true', ()=>{
		button = createMDLComponentInstance(Button,{accent: true})
		expect(button.props.className).toMatch('mdl-button--accent');
	})

	it('should apply class "mdl-js-ripple-effect" when ripple == true', ()=>{
		button = createMDLComponentInstance(Button,{ripple: true})
		expect(button.props.className).toMatch('mdl-js-ripple-effect');
	})

	it('should render child components', ()=>{
		button = createMDLComponentInstance(Button, {}, 'Child')
		expect(button.props.children).toMatch('Child');
	})
})