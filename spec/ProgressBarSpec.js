import ProgressBar from '../lib/ProgressBar'
import createComponent from './util/create-component'

describe('ProgressBar component', () => {

	let progressBar;

	beforeEach(() => {
		progressBar = createComponent(ProgressBar, {})
		//console.log(JSON.stringify(layout))
	});

	it('should apply class "mdl-progress mdl-js-progress"', ()=>{
		expect(progressBar.props.className).toMatch('mdl-progress mdl-js-progress');
	})

	it('should apply class "mdl-js-ripple-effect" when ripple == true', ()=>{
		progressBar = createComponent(ProgressBar, {indeterminate: true})
		expect(progressBar.props.className).toMatch('mdl-progress__indeterminate');
	})


})