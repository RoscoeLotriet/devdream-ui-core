import Switch from '../lib/Switch'
import createComponent from './util/create-component'
import createMDLComponentInstance from './util/create-mdl-component-instance'

describe('Switch component', () => {

	let _switch;

	beforeEach(() => {
		_switch = createMDLComponentInstance(Switch, {})
		//console.log(JSON.stringify(layout))
	});

	it('should apply class "mdl-switch mdl-js-switch"', ()=>{
		expect(_switch.props.className).toMatch('mdl-switch mdl-js-switch');
	})

	it('should apply class "mdl-js-ripple-effect" when ripple == true', ()=>{
		_switch = createMDLComponentInstance(Switch, {ripple: true})
		expect(_switch.props.className).toMatch('mdl-js-ripple-effect');
	})

	it('label "for" attribute should match "MyInputId" when inputId == "MyInputId"', ()=>{
		_switch = createMDLComponentInstance(Switch, {inputId: 'MyInputId'})
		expect(_switch.props.htmlFor).toMatch('MyInputId');
	})

	it('input "id" attribute should match "MyInputId" when inputId == "MyInputId"', ()=>{
		_switch = createMDLComponentInstance(Switch, {inputId: 'MyInputId'})
		expect(_switch.props.children[0].props.id).toMatch('MyInputId');
	})

	it('label should match "MyLabel" when label == "MyLabel"', ()=>{
		_switch = createMDLComponentInstance(Switch, {label: 'MyLabel'})
		expect(_switch.props.children[1].props.children).toMatch('MyLabel');
	})

})