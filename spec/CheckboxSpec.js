import Checkbox from '../lib/Checkbox'
import createComponent from './util/create-component'
import createMDLComponentInstance from './util/create-mdl-component-instance'

describe('Checkbox component', () => {

	let checkBox;

	beforeEach(() => {
		checkBox = createMDLComponentInstance(Checkbox, {})
	});

	it('should apply class "mdl-checkbox mdl-js-checkbox"', ()=>{
		expect(checkBox.props.className).toMatch('mdl-checkbox mdl-js-checkbox');
	})

	it('should apply class "mdl-js-ripple-effect" when ripple == true', ()=>{
		checkBox = createMDLComponentInstance(Checkbox, {ripple: true})
		expect(checkBox.props.className).toMatch('mdl-js-ripple-effect');
	})

	it('label "for" attribute should match "MyInputId" when inputId == "MyInputId"', ()=>{
		checkBox = createMDLComponentInstance(Checkbox, {inputId: 'MyInputId'})
		expect(checkBox.props.htmlFor).toMatch('MyInputId');
	})

	it('input "id" attribute should match "MyInputId" when inputId == "MyInputId"', ()=>{
		checkBox = createMDLComponentInstance(Checkbox, {inputId: 'MyInputId'})
		expect(checkBox.props.children[0].props.id).toMatch('MyInputId');
	})

	it('label should match "MyLabel" when label == "MyLabel"', ()=>{
		checkBox = createMDLComponentInstance(Checkbox, {label: 'MyLabel'})
		expect(checkBox.props.children[1].props.children).toMatch('MyLabel');
	})

})