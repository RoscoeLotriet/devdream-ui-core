import CardTitleText from '../lib/CardTitleText'
import createComponent from './util/create-component'

describe('CardTitleText component', () => {

	let cardTitleText;

	beforeEach(() => {
		cardTitleText = createComponent(CardTitleText, {})
	});

	it('should apply class "mdl-card__title-text"', ()=>{
		expect(cardTitleText.props.className).toMatch('mdl-card__title-text');
	})

	it('should render child components', ()=>{

		cardTitleText = createComponent(CardTitleText, {}, 'Child')
		expect(cardTitleText.props.children).toMatch('Child');

	})
})