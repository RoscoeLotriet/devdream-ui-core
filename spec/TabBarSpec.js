import TabBar from '../lib/TabBar'
import createComponent from './util/create-component'

describe('TabBar component', () => {

	let tabBar;

	beforeEach(() => {
		tabBar = createComponent(TabBar, {})
		
	});

	it('should apply class "mdl-tabs__tab-bar"', ()=>{
		expect(tabBar.props.className).toMatch('mdl-tabs__tab-bar');
	})

	it('should render child components', ()=>{

		tabBar = createComponent(TabBar, {}, 'Child')
		expect(tabBar.props.children).toMatch('Child');

	})

})