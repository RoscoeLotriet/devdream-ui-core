import LayoutTitle from '../lib/LayoutTitle'
import createComponent from './util/create-component'

describe('LayoutTitle component', () => {

	let layoutTitle;

	beforeEach(() => {
		layoutTitle = createComponent(LayoutTitle, {})
	});

	it('should apply class "mdl-layout-title"', () => {
		expect(layoutTitle.props.className).toMatch('mdl-layout-title')
	})

	it('should render child components', () => { 

		layoutTitle = createComponent(LayoutTitle, {}, 'Child')
		expect(layoutTitle.props.children).toMatch('Child')

	})

})