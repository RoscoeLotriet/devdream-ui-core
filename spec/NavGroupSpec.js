import NavGroup from '../lib/NavGroup'
import createComponent from './util/create-component'

describe('NavGroup component', () => {

	let navGroup;

	beforeEach(() => {
		navGroup = createComponent(NavGroup, {})
	});

	it('should apply class "mdl-navigation"', () => {
		expect(navGroup.props.className).toMatch('mdl-navigation');
	})

	it('should render child components', ()=>{

		navGroup = createComponent(NavGroup, {}, 'Child')
		expect(navGroup.props.children).toMatch('Child');

	})

})
