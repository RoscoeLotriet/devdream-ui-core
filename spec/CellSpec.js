import Cell from '../lib/Cell'
import createComponent from './util/create-component'

describe('Cell component', () => {

	let cell;

	beforeEach(() => {
		cell = createComponent(Cell, {})
		
	});

	it('should apply class "mdl-cell"', ()=>{
		expect(cell.props.className).toMatch('mdl-cell');
	})

	it('should apply class "mdl-cell--1-col" when size == 1', ()=>{

		cell = createComponent(Cell, {size: 1})
		expect(cell.props.className).toMatch('mdl-cell--1-col');
	})

	it('should apply class "mdl-cell--6-col-desktop" when desktopSize == 6', ()=>{

		cell = createComponent(Cell, {desktopSize: 6})
		expect(cell.props.className).toMatch('mdl-cell--6-col-desktop');
	})

	it('should apply class "mdl-cell--6-col-tablet" when tabletSize == 6', ()=>{

		cell = createComponent(Cell, {tabletSize: 6})
		expect(cell.props.className).toMatch('mdl-cell--6-col-tablet');
	})

	it('should apply class "mdl-cell--6-col-phone" when phoneSize == 6', ()=>{

		cell = createComponent(Cell, {phoneSize: 6})
		expect(cell.props.className).toMatch('mdl-cell--6-col-phone');
	})

	it('should apply class "mdl-cell--hide-desktop" when hideDesktop == true', ()=>{

		cell = createComponent(Cell, {hideDesktop: true})
		expect(cell.props.className).toMatch('mdl-cell--hide-desktop');
	})

	it('should apply class "mdl-cell--hide-tablet" when hideTablet == true', ()=>{

		cell = createComponent(Cell, {hideTablet: true})
		expect(cell.props.className).toMatch('mdl-cell--hide-tablet');
	})

	it('should apply class "mdl-cell--hide-phone" when hidePhone == true', ()=>{

		cell = createComponent(Cell, {hidePhone: true})
		expect(cell.props.className).toMatch('mdl-cell--hide-phone');
	})

	it('should apply class "mdl-cell--stretch" when stretch == true', ()=>{

		cell = createComponent(Cell, {stretch: true})
		expect(cell.props.className).toMatch('mdl-cell--stretch');
	})

	it('should apply class "mdl-cell--top" when align == top', ()=>{

		cell = createComponent(Cell, {align: 'top'})
		expect(cell.props.className).toMatch('mdl-cell--top');
	})

	it('should apply class "mdl-cell--middle" when align == middle', ()=>{

		cell = createComponent(Cell, {align: 'middle'})
		expect(cell.props.className).toMatch('mdl-cell--middle');
	})

	it('should apply class "mdl-cell--bottom" when align == bottom', ()=>{

		cell = createComponent(Cell, {align: 'bottom'})
		expect(cell.props.className).toMatch('mdl-cell--bottom');
	})

})