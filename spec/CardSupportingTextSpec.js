import CardSupportingText from '../lib/CardSupportingText'
import createComponent from './util/create-component'

describe('CardSupportingText component', () => {

	let cardSupportingText;

	beforeEach(() => {
		cardSupportingText = createComponent(CardSupportingText, {})
	});

	it('should apply class "mdl-card__supporting-text"', ()=>{
		expect(cardSupportingText.props.className).toMatch('mdl-card__supporting-text');
	})

	it('should render child components', ()=>{
		cardSupportingText = createComponent(CardSupportingText, {}, 'Child')
		expect(cardSupportingText.props.children).toMatch('Child');
	})
})