import LayoutHeader from '../lib/LayoutHeader'
import createComponent from './util/create-component'

describe('LayoutHeader component', () => {

	let layoutHeader;

	beforeEach(() => {
		layoutHeader = createComponent(LayoutHeader, {})
		//console.log(JSON.stringify(layout))
	});

	it('should apply class "mdl-layout__header"', () => {
		expect(layoutHeader.props.className).toMatch('mdl-layout__header');
	})

	it('should apply class "mdl-layout__header--scroll" when scroll == true', ()=>{

		layoutHeader = createComponent(LayoutHeader, {scroll: true})
		expect(layoutHeader.props.className).toMatch('mdl-layout__header--scroll');
	})

	it('should apply class "mdl-layout__header--waterfall" when waterfall == true', ()=>{

		layoutHeader = createComponent(LayoutHeader, {waterfall: true})
		expect(layoutHeader.props.className).toMatch('mdl-layout__header--waterfall');
	})

	it('should apply class "mdl-layout__header--transparent" when transparent == true', ()=>{

		layoutHeader = createComponent(LayoutHeader, {transparent: true})
		expect(layoutHeader.props.className).toMatch('mdl-layout__header--transparent');
	})

	it('should render child components', ()=>{

		layoutHeader = createComponent(LayoutHeader, {}, 'Child')
		expect(layoutHeader.props.children).toMatch('Child');

	})

})