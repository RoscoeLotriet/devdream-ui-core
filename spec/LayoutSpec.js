import Layout from '../lib/Layout'
import createComponent from './util/create-component'
import createMDLComponentInstance from './util/create-mdl-component-instance'

describe('Layout component', () => {

	let layout;

	beforeEach(() => {
		layout = createMDLComponentInstance(Layout, {})
		//console.log(JSON.stringify(layout))
	});

	it('should apply classes "mdl-layout mdl-js-layout"', ()=>{
		expect(layout.props.className).toMatch('mdl-layout mdl-js-layout');
	})

	it('should apply class "mdl-layout--overlay-drawer-button" when overlayDrawerButton == true', ()=>{

		layout = createMDLComponentInstance(Layout, {overlayDrawerButton: true})
		expect(layout.props.className).toMatch('mdl-layout--overlay-drawer-button');
	})

	it('should apply class "mdl-layout--fixed-drawer" when fixedDrawer == true', ()=>{

		layout = createMDLComponentInstance(Layout, {fixedDrawer: true})
		expect(layout.props.className).toMatch('mdl-layout--fixed-drawer');
	})

	it('should apply class "mdl-layout--fixed-header" when fixedHeader == true', ()=>{

		layout = createMDLComponentInstance(Layout, {fixedHeader: true})
		expect(layout.props.className).toMatch('mdl-layout--fixed-header');
	})

	it('should apply class "mdl-layout--large-screen-only" when largeScreenOnly == true', ()=>{

		layout = createMDLComponentInstance(Layout, {largeScreenOnly: true})
		expect(layout.props.className).toMatch('mdl-layout--large-screen-only');
	})

	it('should render child components', ()=>{

		layout = createMDLComponentInstance(Layout, {}, 'Child')
		expect(layout.props.children).toMatch('Child');

	})
})