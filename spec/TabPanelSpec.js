import TabPanel from '../lib/TabPanel'
import createComponent from './util/create-component'

describe('TabPanel component', () => {

	let tabPanel;

	beforeEach(() => {
		tabPanel = createComponent(TabPanel, {})
		
	});

	it('should apply class "mdl-tabs__panel"', ()=>{
		expect(tabPanel.props.className).toMatch('mdl-tabs__panel');
	})


	it('should apply class "is-active" when active == true', ()=>{

		tabPanel = createComponent(TabPanel, {active: true})
		expect(tabPanel.props.className).toMatch('is-active');
	})

	it('should have id matching myTabId when tabId == "myTabId"', ()=>{

		tabPanel = createComponent(TabPanel, {tabId: 'myTabId'})
		expect(tabPanel.props.id).toMatch('myTabId');
	})

	it('should render child components', ()=>{

		tabPanel = createComponent(TabPanel, {}, 'Child')
		expect(tabPanel.props.children).toMatch('Child');

	})

})