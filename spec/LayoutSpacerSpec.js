import LayoutSpacer from '../lib/LayoutSpacer'
import createComponent from './util/create-component'

describe('LayoutSpacer component', () => {

	let layoutSpacer;

	beforeEach(() => {
		layoutSpacer = createComponent(LayoutSpacer, {})
	});

	it('should apply class "mdl-layout-spacer"', () => {
		expect(layoutSpacer.props.className).toMatch('mdl-layout-spacer')
	})

})