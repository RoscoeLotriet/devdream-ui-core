import Badge from '../lib/Badge'
import createComponent from './util/create-component'

describe('Badge component', () => {

	let badge;

	beforeEach(() => {
		badge = createComponent(Badge, {})
		
	});

	it('should apply class "mdl-badge"', ()=>{
		expect(badge.props.className).toMatch('mdl-badge');
	})

	it('should apply class "mdl-badge--no-background" when noBackground == true', ()=>{

		badge = createComponent(Badge, {noBackground: true})
		expect(badge.props.className).toMatch('mdl-badge--no-background');
	})

	it('should apply attribute "data-badge" when value has text', ()=>{

		badge = createComponent(Badge, {value: "100"})
		expect(badge.props['data-badge']).toMatch('100');
	})

	it('should render child components', ()=>{

		badge = createComponent(Badge, {}, 'Child')
		expect(badge.props.children).toMatch('Child');

	})
})