import NavLink from '../lib/NavLink'
import createComponent from './util/create-component'

describe('NavLink component', () => {

	let navLink;

	beforeEach(() => {
		navLink = createComponent(NavLink, {})
	});

	it('should apply class "mdl-navigation__link"', () => {
		expect(navLink.props.className).toMatch('mdl-navigation__link');
	})

	it('should render child components', ()=>{

		navLink = createComponent(NavLink, {}, 'Child')
		expect(navLink.props.children).toMatch('Child');

	})

})
