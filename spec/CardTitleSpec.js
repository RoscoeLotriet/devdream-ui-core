import CardTitle from '../lib/CardTitle'
import createComponent from './util/create-component'

describe('CardTitle component', () => {

	let cardTitle;

	beforeEach(() => {
		cardTitle = createComponent(CardTitle, {})
		//console.log(JSON.stringify(layout))
	});

	it('should apply class "mdl-card__title"', ()=>{
		expect(cardTitle.props.className).toMatch('mdl-card__title');
	})

	it('should render child components', ()=>{

		cardTitle = createComponent(CardTitle, {}, 'Child')
		expect(cardTitle.props.children).toMatch('Child');

	})
})