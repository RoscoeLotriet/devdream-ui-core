import Tab from '../lib/Tab'
import createComponent from './util/create-component'

describe('Tab component', () => {

	let tab;

	beforeEach(() => {
		tab = createComponent(Tab, {})
		
	});

	it('should apply class "mdl-tabs__tab"', ()=>{
		expect(tab.props.className).toMatch('mdl-tabs__tab');
	})


	it('should have href matching #myTabId when tabId == "myTabId"', ()=>{

		tab = createComponent(Tab, {tabId: 'myTabId'})
		expect(tab.props.href).toMatch('#myTabId');
	})

	it('should render child components', ()=>{

		tab = createComponent(Tab, {}, 'Child')
		expect(tab.props.children).toMatch('Child');

	})

})