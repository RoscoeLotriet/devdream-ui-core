import TextField from '../lib/TextField'
import createComponent from './util/create-component'
import createMDLComponentInstance from './util/create-mdl-component-instance'

describe('TextField component', () => {

	let textField;

	beforeEach(() => {
		textField = createMDLComponentInstance(TextField, {})
		//console.log(JSON.stringify(layout))
	});

	it('should apply class "mdl-textfield mdl-js-textfield"', ()=>{
		expect(textField.props.className).toMatch('mdl-textfield mdl-js-textfield');
	})

	it('should apply class "mdl-textfield--floating-label" when floatingLabel == true', ()=>{
		textField = createMDLComponentInstance(TextField, {floatingLabel: true})
		expect(textField.props.className).toMatch('mdl-textfield--floating-label');
	})

	it('should apply class "mdl-textfield--expandable" when expandable == true', ()=>{
		textField = createMDLComponentInstance(TextField, {expandable: true})
		expect(textField.props.className).toMatch('mdl-textfield--expandable');
	})

	it('name should match "MyInputName" when name == "MyInputName"', ()=>{
		textField = createMDLComponentInstance(TextField, {name: 'MyInputName'})
		expect(textField.props.children[0].props.id).toMatch('MyInputName');
	})

	it('label "for" attribute should match "MyInputName" when name == "MyInputName"', ()=>{
		textField = createMDLComponentInstance(TextField, {name: 'MyInputName'})
		expect(textField.props.children[1].props.htmlFor).toMatch('MyInputName');
	})

	it('label should match "MyLabel" when label == "MyLabel"', ()=>{
		textField = createMDLComponentInstance(TextField, {label: 'MyLabel'})
		expect(textField.props.children[1].props.children).toMatch('MyLabel');
	})

	it('pattern should match "[A-Z,a-z, ]*" when pattern == "[A-Z,a-z, ]*"', ()=>{
		textField = createMDLComponentInstance(TextField, {pattern: '[A-Z,a-z, ]*'})
		expect(textField.props.children[0].props.pattern).toMatch('[A-Z,a-z, ]*');
	})

	it('error should match "MyError" when error == "MyError"', ()=>{
		textField = createMDLComponentInstance(TextField, {error: 'MyError'})
		expect(textField.props.children[2].props.children).toMatch('MyError');
	})
	

})