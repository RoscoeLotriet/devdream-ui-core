import CardMedia from '../lib/CardMedia'
import createComponent from './util/create-component'

describe('CardMedia component', () => {

	let cardMedia;

	beforeEach(() => {
		cardMedia = createComponent(CardMedia, {})
	});

	it('should apply class "mdl-card__media"', ()=>{
		expect(cardMedia.props.className).toMatch('mdl-card__media');
	})

	it('should render child components', ()=>{

		cardMedia = createComponent(CardMedia, {}, 'Child')
		expect(cardMedia.props.children).toMatch('Child');

	})
})