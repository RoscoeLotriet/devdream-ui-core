import DefaultLayout from '../lib/DefaultLayout'
import createComponent from './util/create-component'

describe('DefaultLayout component', () => {

	let defaultLayout;

	beforeEach(() => {
		defaultLayout = createComponent(DefaultLayout, {})
		// console.log(JSON.stringify(defaultLayout.props.children[0].props.children[1].props.children))
	});

	it('overlayDrawerButton prop should be true', ()=>{
		defaultLayout = createComponent(DefaultLayout, {overlayDrawerButton: true})
		expect(defaultLayout.props.overlayDrawerButton).toBe(true);
	})

	it('fixedHeader prop should be true', ()=>{
		defaultLayout = createComponent(DefaultLayout, {fixedHeader: true})
		expect(defaultLayout.props.fixedHeader).toBe(true);
	})

	it('header prop on LayoutHeader should be true', ()=>{
		defaultLayout = createComponent(DefaultLayout, {waterfall: true})
		expect(defaultLayout.props.children[0].props.waterfall).toBe(true);
	})

	it('LayoutHeader should render pageTitle', ()=>{

		defaultLayout = createComponent(DefaultLayout, {pageTitle: 'My Page Title'})

		const title = defaultLayout.props.children[0].props.children[1].props.children.props.children;

		expect(title).toEqual('My Page Title');
	})

	it('LayoutDrawer should render drawTitle', ()=>{

		defaultLayout = createComponent(DefaultLayout, {drawerTitle: 'My Drawer Title'})

		const title = defaultLayout.props.children[1].props.children[0].props.children;

		expect(title).toEqual('My Drawer Title');
	})

	it('LayoutDrawer should render navGroups', ()=>{

		defaultLayout = createComponent(DefaultLayout, {navGroups: ['My NavGroups']})

		const navGroups = defaultLayout.props.children[1].props.children[1];

		expect(navGroups).toEqual(['My NavGroups']);
	})

	it('LayoutContent should render children', ()=>{

		defaultLayout = createComponent(DefaultLayout, {}, 'My Child')

		const content = defaultLayout.props.children[2].props.children;

		expect(content).toEqual('My Child');
	})

})