import CardActions from '../lib/CardActions'
import createComponent from './util/create-component'

describe('CardActions component', () => {

	let cardActions;

	beforeEach(() => {
		cardActions = createComponent(CardActions, {})
	});

	it('should apply class "mdl-card__actions"', ()=>{
		expect(cardActions.props.className).toMatch('mdl-card__actions');
	})

	it('should render child components', ()=>{
		cardActions = createComponent(CardActions, {}, 'Child')
		expect(cardActions.props.children).toMatch('Child');
	})
})