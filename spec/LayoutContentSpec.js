import LayoutContent from '../lib/LayoutContent'
import createComponent from './util/create-component'

describe('LayoutContent component', () => {

	let layoutContent;

	beforeEach(() => {
		layoutContent = createComponent(LayoutContent, {})
	});

	it('should apply class "mdl-layout__content"', () => {
		expect(layoutContent.props.className).toMatch('mdl-layout__content');
	})

	it('should render child components', ()=>{

		layoutContent = createComponent(LayoutContent, {}, 'Child')
		expect(layoutContent.props.children).toMatch('Child');

	})

})