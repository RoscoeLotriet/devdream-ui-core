import Card from '../lib/Card'
import createComponent from './util/create-component'

describe('Card component', () => {

	let card;

	beforeEach(() => {
		card = createComponent(Card, {})
		//console.log(JSON.stringify(layout))
	});

	it('should apply class "mdl-card"', ()=>{
		expect(card.props.className).toMatch('mdl-card');
	})

	it('should render child components', ()=>{

		card = createComponent(Card, {}, 'Child')
		expect(card.props.children).toMatch('Child');

	})
})