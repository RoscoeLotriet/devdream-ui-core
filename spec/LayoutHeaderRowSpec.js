import LayoutHeaderRow from '../lib/LayoutHeaderRow'
import createComponent from './util/create-component'

describe('LayoutHeaderRow component', () => {

	let layoutHeaderRow;

	beforeEach(() => {
		layoutHeaderRow = createComponent(LayoutHeaderRow, {})
	});

	it('should apply class "mdl-layout__header-row"', () => {
		expect(layoutHeaderRow.props.className).toMatch('mdl-layout__header-row');
	})

	it('should render child components', ()=>{

		layoutHeaderRow = createComponent(LayoutHeaderRow, {}, 'Child')
		expect(layoutHeaderRow.props.children).toMatch('Child');

	})

})