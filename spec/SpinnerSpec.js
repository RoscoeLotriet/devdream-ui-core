import Spinner from '../lib/Spinner'
import createComponent from './util/create-component'
import createMDLComponentInstance from './util/create-mdl-component-instance'

describe('Spinner component', () => {

	let spinner;

	beforeEach(() => {
		spinner = createMDLComponentInstance(Spinner, {})
		//console.log(JSON.stringify(layout))
	});

	it('should apply class "mdl-spinner mdl-js-spinner"', ()=>{
		expect(spinner.props.className).toMatch('mdl-spinner mdl-js-spinner');
	})

	it('should apply class "mdl-spinner--single-color" when singleColor == true', ()=>{
		spinner = createMDLComponentInstance(Spinner, {singleColor: true})
		expect(spinner.props.className).toMatch('mdl-spinner--single-color');
	})

	it('should apply class "is-active" when active == true', ()=>{
		spinner = createMDLComponentInstance(Spinner, {active: true})
		expect(spinner.props.className).toMatch('is-active');
	})


})