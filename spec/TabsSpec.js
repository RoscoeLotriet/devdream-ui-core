import Tabs from '../lib/Tabs'
import createComponent from './util/create-component'

describe('Tabs component', () => {

	let tabs;

	beforeEach(() => {
		tabs = createComponent(Tabs, {})
		
	});

	it('should apply class "mdl-tabs mdl-js-tabs"', ()=>{
		expect(tabs.props.className).toMatch('mdl-tabs mdl-js-tabs');
	})

	it('should apply class "mdl-js-ripple-effect" when ripple == true', ()=>{

		tabs = createComponent(Tabs, {ripple: true})
		expect(tabs.props.className).toMatch('mdl-js-ripple-effect');
	})

	it('should render child components', ()=>{

		tabs = createComponent(Tabs, {}, 'Child')
		expect(tabs.props.children).toMatch('Child');

	})

})