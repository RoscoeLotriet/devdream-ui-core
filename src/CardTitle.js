'use strict';
import React from 'react'
import classnames from 'classnames'
 
const baseClasses = {
  'mdl-card__title': true
};

const CardTitle = React.createClass({

  propTypes: {
    className: React.PropTypes.string
  },

  componentDidMount(){

  },

  componentWillUnmount(){

  },

  render(){
    const {
      children,
      className
    } = this.props;

    const classes = classnames(baseClasses, {}, className);

    return (
      <div {...this.props} className={classes}>
        {children}
      </div>
    );
  }
})

export default CardTitle;