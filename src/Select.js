'use strict';
import React, {PropTypes} from 'react'
import classnames from 'classnames'
import mdlUpgrade from './common/mdlUpgrade'
 
const baseClasses = {
  'mdl-select': true,
  'mdl-js-select': true
};

class Select extends React.Component {

  static propTypes = {
    className: PropTypes.string,
    name: PropTypes.string,
    value:  PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ]),
    onChange: PropTypes.func,
    onKeyup: PropTypes.func,
    floatingLabel: PropTypes.bool,
    expandable: PropTypes.bool,
    label: PropTypes.string,
    pattern: PropTypes.string,
    error: PropTypes.string

  }

  _handleChange = (e) => {
    this.props.onChange(e.target.value);
  }

  componentDidUpdate(prevProps) {
    if(React.findDOMNode(this).MaterialSelect){
      React.findDOMNode(this).MaterialSelect.checkDirty();
    }
  }

  render(){
    const {
      children,
      className,
      name,
      value,
      onChange,
      floatingLabel,
      label,
      addBlank,
      valueLink,
      options
    } = this.props;


    const classes = classnames(baseClasses, {
    	'mdl-select--floating-label': floatingLabel
    }, className);

    if(addBlank){
      options.unshift({value: '', name: ''});
    }

    const field = ( <div className={classes}>
        <select className="mdl-select__input" valueLink={valueLink} ref={name} id={name} name={name} onChange={onChange}>
	        {options.map((option)=>{
	        	return (<option value={option.value}>{option.name}</option>)
	        })}
        </select>
        <label className="mdl-select__label" key={name+"label"} htmlFor={name}>{label}</label>
        </div> )


    return field
  }
}

export default mdlUpgrade(Select);