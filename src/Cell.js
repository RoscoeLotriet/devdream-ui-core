'use strict';
import React from 'react'
import classnames from 'classnames'
 
const baseClasses = {
  'mdl-cell': true
};

const Cell = React.createClass({

  propTypes: {
    className: React.PropTypes.string
  },

  componentDidMount(){

  },

  componentWillUnmount(){

  },

  render(){
    const {
      children,
      className,
      size,
      desktopSize,
      tabletSize,
      phoneSize,
      hideDesktop,
      hideTablet,
      hidePhone,
      stretch,
      align
    } = this.props;

    let mdlClasses = {}
  
    if(size){
    	const sizeClass = 'mdl-cell--'+size+'-col'
    	mdlClasses[sizeClass] = true
    }

	if(desktopSize){
    	const sizeClass = 'mdl-cell--'+desktopSize+'-col-desktop'
    	mdlClasses[sizeClass] = true
    }

    if(tabletSize){
    	const sizeClass = 'mdl-cell--'+tabletSize+'-col-tablet'
    	mdlClasses[sizeClass] = true
    }

    if(phoneSize){
    	const sizeClass = 'mdl-cell--'+phoneSize+'-col-phone'
    	mdlClasses[sizeClass] = true
    }

    if(align){
    	const alignClass = 'mdl-cell--'+align
    	mdlClasses[alignClass] = true
    }

    mdlClasses['mdl-cell--hide-desktop'] = hideDesktop
    mdlClasses['mdl-cell--hide-tablet'] = hideTablet
    mdlClasses['mdl-cell--hide-phone'] = hidePhone
    mdlClasses['mdl-cell--stretch'] = stretch
   
    const classes = classnames(baseClasses, mdlClasses, className);

    return (
      <div {...this.props} className={classes}>
        {children}
      </div>
    );
  }
})

export default Cell;