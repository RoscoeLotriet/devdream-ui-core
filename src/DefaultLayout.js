'use strict';
import React from 'react'
import Layout from './Layout' 
import LayoutIcon from './LayoutIcon'
import LayoutHeader from './LayoutHeader' 
import LayoutHeaderRow from './LayoutHeaderRow'
import LayoutTitle from './LayoutTitle'
import LayoutDrawer from './LayoutDrawer'
import LayoutContent from './LayoutContent' 

const DefaultLayout = React.createClass({

  propTypes: {
    overlayDrawerButton: React.PropTypes.bool,
    fixedHeader: React.PropTypes.bool,
    fixedDrawer: React.PropTypes.bool,
    waterfall: React.PropTypes.bool,
    pageTitle: React.PropTypes.string,
    drawerTitle: React.PropTypes.string,
    navGroups: React.PropTypes.array
  },

  componentDidMount(){

  },

  componentWillUnmount(){

  },

  render(){
    
    const {
      children,
      overlayDrawerButton,
      fixedHeader,
      fixedDrawer,
      waterfall,
      pageTitle,
      drawerTitle,
      navGroups
    } = this.props;

    return (
      <Layout overlayDrawerButton={overlayDrawerButton} fixedHeader={fixedHeader} fixedDrawer={fixedDrawer}>
    		<LayoutHeader waterfall={waterfall}>
          <LayoutIcon></LayoutIcon>
    	 		<LayoutHeaderRow>
    				<LayoutTitle>{pageTitle}</LayoutTitle>
    			</LayoutHeaderRow>
        </LayoutHeader>
    		<LayoutDrawer>
    			<LayoutTitle>{drawerTitle}</LayoutTitle>
            {navGroups}
    		</LayoutDrawer>
    		<LayoutContent>{children}</LayoutContent>
      </Layout>
    );
  }
})

export default DefaultLayout;