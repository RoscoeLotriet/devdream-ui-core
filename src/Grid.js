'use strict';
import React from 'react'
import classnames from 'classnames'
 
const baseClasses = {
  'mdl-grid': true
};

const Grid = React.createClass({

  propTypes: {
    className: React.PropTypes.string
  },

  componentDidMount(){

  },

  componentWillUnmount(){

  },

  render(){
    const {
      children,
      className,
      noSpacing
    } = this.props;

    const classes = classnames(baseClasses, {
    	'mdl-grid--no-spacing' : noSpacing
    }, className);

    return (
      <div {...this.props} className={classes}>
        {children}
      </div>
    );
  }
})

export default Grid;