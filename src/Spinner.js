'use strict';
import React from 'react'
import classnames from 'classnames'
import mdlUpgrade from './common/mdlUpgrade'
 
const baseClasses = {
  'mdl-spinner': true,
  'mdl-js-spinner': true
};

class Spinner extends React.Component {

  static propTypes = {
    className: React.PropTypes.string
  }

  render(){
    const {
      children,
      className,
      active,
      singleColor
    } = this.props;

    const classes = classnames(baseClasses, {
    	'is-active': active,
    	'mdl-spinner--single-color': singleColor
    }, className);

    return (
      <div className={classes}></div>
    );
  }
}

export default mdlUpgrade(Spinner);