'use strict';
import React from 'react'
import classnames from 'classnames'
import mdlUpgrade from './common/mdlUpgrade'
 
const baseClasses = {
  'mdl-checkbox': true,
  'mdl-js-checkbox': true
};

class Checkbox extends React.Component{

  static propTypes = {
    className: React.PropTypes.string
  }

  render(){
    const {
      children,
      className,
      inputId,
      ripple,
      label,
      checkedLink
    } = this.props;

    const classes = classnames(baseClasses, {
    	'mdl-js-ripple-effect': ripple
    }, className);

    return (
      <label {...this.props} className={classes} htmlFor={inputId}>
        <input type="checkbox" id={inputId} className="mdl-checkbox__input" checkedLink={checkedLink}/>
        <span className="mdl-checkbox__label">{label}</span>
      </label>
    );
  }
}

export default mdlUpgrade(Checkbox);