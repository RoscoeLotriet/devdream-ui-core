'use strict';
import React from 'react'
import mdl from 'devdream-material-design-lite'
import classnames from 'classnames'
 
const baseClasses = {
  'mdl-tabs': true,
  'mdl-js-tabs' : true  
};

const Tabs = React.createClass({

  propTypes: {
    className: React.PropTypes.string
  },

  componentDidMount(){

    // const { ripple } = this.props;

    // const node = React.findDOMNode(this);
    // mdl.upgradeElement(node, 'MaterialTabs');

    // if(ripple){
    //   mdl.upgradeElement(node, 'MaterialRipple');
    // }

  },

  componentWillUnmount(){

  },

  render(){
    const {
      children,
      className,
      ripple
    } = this.props;

    const classes = classnames(baseClasses, {
    	'mdl-js-ripple-effect': ripple
    }, className);

    return (
      <div {...this.props} className={classes}>
        {children}
      </div>
    );
  }
})

export default Tabs;