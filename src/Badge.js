'use strict';
import React from 'react'
import classnames from 'classnames'
 
const baseClasses = {
  'mdl-badge': true
};

const Badge = React.createClass({

  propTypes: {
    className: React.PropTypes.string,
    noBackground: React.PropTypes.bool,
    value: React.PropTypes.string
  },

  componentDidMount(){

  },

  componentWillUnmount(){

  },

  render(){
    const {
      children,
      className,
      noBackground,
      value,
    } = this.props;

    const classes = classnames(baseClasses, {
    	'mdl-badge--no-background' : noBackground
    }, className);

    return (
      <div {...this.props} className={classes} data-badge={value}>
        {children}
      </div>
    );
  }
})

export default Badge;