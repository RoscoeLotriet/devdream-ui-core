'use strict';
import React from 'react'


const FormSelect = React.createClass({

  propTypes: {
    className: React.PropTypes.string
  },

  getInitialState() {
    return {
      value: this.props.value || ''
    }
  },

  componentDidMount(){
     // If we use the required prop we add a validation rule
    // that ensures there is a value. The input
    // should not be valid with empty value
    if (this.props.required) {
      this.props.validations = this.props.validations ? this.props.validations + ',' : '';
      this.props.validations += 'isValue';
    }
    
    this.props.attachToForm(this);
  },

  componentWillMount() {
  
   
  },

  componentWillUnmount() {
    this.props.detachFromForm(this); // Detaching if unmounting
  },

  // Whenever the input changes we update the value state
  // of this component
  setValue(event) {
    this.setState({
      value: event.nativeEvent.target.value
      
      // When the value changes, wait for it to propagate and
      // then validate the input
    }, () => {
      this.props.validate(this);
    }.bind(this));
  },

  render(){

  	// We create variables that states how the input should be marked.
    // Should it be marked as valid? Should it be marked as required?
    var markAsValid = this.state.isValid;
    var markAsRequired = this.props.required && !this.state.value;

    return (
    	<select name={this.props.name} {...this.props} onChange={this.setValue}>
    		{this.props.children}
	   	</select>
    );

  }
})

export default FormSelect;