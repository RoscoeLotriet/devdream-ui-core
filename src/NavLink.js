'use strict';
import React from 'react'
import classnames from 'classnames'

 
const baseClasses = {
  'mdl-navigation__link': true
};

const NavLink = React.createClass({

  propTypes: {
    className: React.PropTypes.string
  },

  componentDidMount(){

  },

  componentWillUnmount(){

  },

  render(){
    const {
      children,
      className
    } = this.props;

    const classes = classnames(baseClasses, {}, className);

    return (
      <a {...this.props} className={classes}>
        {children}
      </a>
    );
  }
})

export default NavLink;