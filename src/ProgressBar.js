'use strict';
import React from 'react'
import mdl from 'devdream-material-design-lite'
import classnames from 'classnames'
 
const baseClasses = {
  'mdl-progress': true,
  'mdl-js-progress': true
};

const ProgressBar = React.createClass({

  propTypes: {
    className: React.PropTypes.string
  },

  componentDidMount(){

  	const {
      indeterminate,
      progress,
      buffer
    } = this.props;

    const node = React.findDOMNode(this);
   
   	if(!indeterminate){ 
	    node.addEventListener('mdl-componentupgraded', (foo) => {
	    	console.log(foo);
        node.MaterialProgress.setProgress(progress);
	    	node.MaterialProgress.setBuffer(buffer);
	    })
    }

    // mdl.upgradeElement(node, 'MaterialProgress');

  },

  componentWillUnmount(){

  },

  render(){
    const {
      children,
      className,
      indeterminate,
      progress,
      buffer
    } = this.props;

    const classes = classnames(baseClasses, {
    	'mdl-progress__indeterminate': indeterminate
    }, className);

    return (
      <div className={classes}></div>
    );
  }
})

export default ProgressBar;