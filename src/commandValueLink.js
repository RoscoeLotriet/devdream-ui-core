export default function makeValueLink(component, command, key){
	return {
	    value: component.state[command][key],
	    requestChange: function(newValue) {
	      let newState = {};
	      newState[command] = component.state[command]
	      newState[command][key] = newValue;
	      component.setState(newState);
	    }
  	}
}