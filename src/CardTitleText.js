'use strict';
import React from 'react'
import classnames from 'classnames'
 
const baseClasses = {
  'mdl-card__title-text': true
};

const CardTitleText = React.createClass({

  propTypes: {
    className: React.PropTypes.string
  },

  componentDidMount(){

  },

  componentWillUnmount(){

  },

  render(){
    const {
      children,
      className
    } = this.props;

    const classes = classnames(baseClasses, {}, className);

    return (
      <h1 {...this.props} className={classes}>
        {children}
      </h1>
    );
  }
})

export default CardTitleText;