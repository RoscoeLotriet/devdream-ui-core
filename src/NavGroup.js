'use strict';
import React from 'react'
import classnames from 'classnames'

 
const baseClasses = {
  'mdl-navigation': true
};

const NavGroup = React.createClass({

  propTypes: {
    className: React.PropTypes.string
  },

  componentDidMount(){

  },

  componentWillUnmount(){

  },

  render(){
    const {
      children,
      className
    } = this.props;

    const classes = classnames(baseClasses, {}, className);

    return (
      <nav {...this.props} className={classes}>
        {children}
      </nav>
    );
  }
})

export default NavGroup;