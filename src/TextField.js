'use strict';
import React, {PropTypes} from 'react'
import classnames from 'classnames'
import mdlUpgrade from './common/mdlUpgrade'
 
const baseClasses = {
  'mdl-textfield': true,
  'mdl-js-textfield': true
};

class TextField extends React.Component {

  static propTypes = {
    className: PropTypes.string,
    name: PropTypes.string,
    value:  PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ]),
    onChange: PropTypes.func,
    onKeyup: PropTypes.func,
    floatingLabel: PropTypes.bool,
    expandable: PropTypes.bool,
    label: PropTypes.string,
    pattern: PropTypes.string,
    error: PropTypes.string

  }

  componentDidUpdate(prevProps) {
    React.findDOMNode(this).MaterialTextfield.checkDirty();
    if(
        this.props.required !== prevProps.required ||
        this.props.pattern !== prevProps.pattern
    ) {
        React.findDOMNode(this).MaterialTextfield.checkValidity();
    }
    if(this.props.disabled !== prevProps.disabled) {
        React.findDOMNode(this).MaterialTextfield.checkDisabled();
    }
    if(this.props.error && !this.props.pattern) {
        // At every re-render, mdl will set 'is-invalid' class according to the 'pattern' props validity
        // If we want to force the error display, we have to override mdl 'is-invalid' value.
        var elt = React.findDOMNode(this);
        elt.className = classnames(elt.className, 'is-invalid');
    }
  }

  _handleChange = (e) => {
    this.props.onChange(e.target.value);
  }

  render(){
    const {
      children,
      className,
      name,
      value,
      onChange,
      onKeyup,
      onBlur,
      floatingLabel,
      expandable,
      label,
      pattern,
      error,
      valueLink
    } = this.props;


    const classes = classnames(baseClasses, {
    	'mdl-textfield--floating-label': floatingLabel,
    	'mdl-textfield--expandable': expandable
    }, className);

    const field = ( <div className={classes}>
        <input key={name+"input"} className="mdl-textfield__input" type="text" ref={name} id={name} name={name} value={value} onChange={onChange} onKeyup={onKeyup} onBlur={onBlur} valueLink={valueLink}/>
        <label key={name+"label"} className="mdl-textfield__label" htmlFor={name}>{label}</label>
        <span key={name+"error"} className="mdl-textfield__error">{error}</span>
      </div> )


    return field
  }
}

export default mdlUpgrade(TextField);