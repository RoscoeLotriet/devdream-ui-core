'use strict';
import React from 'react'
import classnames from 'classnames'
 
const baseClasses = {
  'mdl-tabs__tab': true
};

const Tab = React.createClass({

  propTypes: {
    className: React.PropTypes.string
  },

  componentDidMount(){

  },

  componentWillUnmount(){

  },

  render(){
    const {
      children,
      className,
      tabId
    } = this.props

    const classes = classnames(baseClasses, {}, className)

    let tabAnchorId = '#'+tabId

    return (
      <a href={tabAnchorId} {...this.props} className={classes}>
        {children}
      </a>
    );
  }
})

export default Tab;