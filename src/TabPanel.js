'use strict';
import React from 'react'
import classnames from 'classnames'
 
const baseClasses = {
  'mdl-tabs__panel': true
};

const TabPanel = React.createClass({

  propTypes: {
    className: React.PropTypes.string
  },

  componentDidMount(){

  },

  componentWillUnmount(){

  },

  render(){
    const {
      children,
      className,
      active,
      tabId
    } = this.props

    const classes = classnames(baseClasses, {
    	'is-active' : active
    }, className)

    return (
      <div id={tabId} {...this.props} className={classes}>
        {children}
      </div>
    );
  }
})

export default TabPanel;