'use strict';
import React from 'react'
import classnames from 'classnames'
 
const baseClasses = {
  'mdl-layout__header': true
};

const LayoutHeader = React.createClass({

  propTypes: {
    className: React.PropTypes.string,
    scroll: React.PropTypes.bool,
    waterfall: React.PropTypes.bool,
    transparent: React.PropTypes.bool
  },

  componentDidMount(){

  },

  componentWillUnmount(){

  },

  render(){
    const {
      children,
      className,
      scroll,
      waterfall,
      transparent
    } = this.props;

    const classes = classnames(baseClasses, {
      'mdl-layout__header--scroll': scroll,
      'mdl-layout__header--waterfall': waterfall,
      'mdl-layout__header--transparent': transparent
    }, className);

    return (
      <header {...this.props} className={classes}>
        {children}
      </header>
    );
  }
})

export default LayoutHeader;