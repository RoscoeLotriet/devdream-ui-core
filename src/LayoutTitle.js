'use strict';
import React from 'react'
import classnames from 'classnames'

 
const baseClasses = {
  'mdl-layout-title': true
};

const LayoutTitle = React.createClass({

  propTypes: {
    className: React.PropTypes.string
  },

  componentDidMount(){

  },

  componentWillUnmount(){

  },

  render(){
    const {
      children,
      className
    } = this.props;

    const classes = classnames(baseClasses, {}, className);

    return (
      <span {...this.props} className={classes}>
        {children}
      </span>
    );
  }
})

export default LayoutTitle;