'use strict';
import React from 'react'
import classnames from 'classnames'
 
const baseClasses = {
  'mdl-layout__content': true
};

const LayoutContent = React.createClass({

  propTypes: {
    className: React.PropTypes.string
  },

  componentDidMount(){

  },

  componentWillUnmount(){

  },

  render(){
    const {
      children,
      className
    } = this.props;

    const classes = classnames(baseClasses, {}, className);

    return (
      <main {...this.props} className={classes}>
        {children}
      </main>
    );
  }
})

export default LayoutContent;