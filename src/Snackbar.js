'use strict';
import React from 'react'
import classnames from 'classnames'
import mdlUpgrade from './common/mdlUpgrade'

const baseClasses = {
  'mdl-js-snackbar': true
}

class Snackbar extends React.Component {

	static propTypes = {
	    show: React.PropTypes.bool,
	    message: React.PropTypes.string,
	    actionHandler: React.PropTypes.func,
	    actionText: React.PropTypes.string,
	    timeout: React.PropTypes.number
 	}

 	componentDidUpdate(prevProps){
 		const {
	      show,
	      message,
	      actionHandler,
	      actionText,
	      timeout
	    } = this.props;

	    if(show){
	    	const data = {
	    		message: message,
	    		actionHandler: actionHandler,
	    		actionText: actionText,
	    		timeout: timeout
	    	}

	    	React.findDOMNode(this).MaterialSnackbar.showSnackbar(data);
	    }
 	}

 	render(){

	    const classes = classnames(baseClasses, {});

	    return (<div className={classes}></div>);
  }
}

export default mdlUpgrade(Snackbar)