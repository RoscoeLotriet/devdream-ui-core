'use strict';
import React from 'react'
import Card from './Card' 
import CardTitle from './CardTitle' 
import CardTitleText from './CardTitleText'
import CardSupportingText from './CardSupportingText' 
import CardActions from './CardActions'

const DefaultCard = React.createClass({

  propTypes: {    
    className: React.PropTypes.string,
  },

  componentWillMount(){

  },

  componentDidMount(){

  },

  componentWillUnmount(){

  },

  render(){
    const {
      children,
      className,
      label,
      supportingText,
      actions
    } = this.props;


    return (
      <Card className="mdl-shadow--4dp command-card">
          <CardTitle className="mdl-card--expand">
              <CardTitleText>{label}</CardTitleText>
          </CardTitle>
          <CardSupportingText>
           {supportingText}
           {children} 
          </CardSupportingText>
          <CardActions className="mdl-card--border">
              {actions}
          </CardActions>
      </Card>
    );
  }
})

export default DefaultCard;