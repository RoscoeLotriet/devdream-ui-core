'use strict';
import React from 'react'
import validator from 'validator'

const FormComponents = ["FormSelect", "FormTextField"];

const Form = React.createClass({

	getInitialState() {
		return {
			isValid: true
		}
	},

	componentWillMount() {
		this.inputs = {};
		this.model = {};
	},

	// When the form loads we validate it
	componentDidMount() {
		this.validateForm();
	},

	validate(component){
		// If no validations property, do not validate
		if (!component.props.validations) {
			return;
		}

		// We initially set isValid to true and then flip it if we
	    // run a validator that invalidates the input
	    let isValid = true;

	    // We only validate if the input has value or if it is required
	    if(component.state.value || component.props.required){

	    	// We split on comma to iterate the list of validation rules
	    	component.props.validations.split(',').forEach((validation)=>{

	    		// By splitting on ":"" we get an array of arguments that we pass
		        // to the validator. ex.: isLength:5 -> ['isLength', '5']

		        let args = validation.split(':');

		        // We remove the top item and are left with the method to
		        // call the validator with ['isLength', '5'] -> 'isLength'
		        let validateMethod = args.shift();

		        // We use JSON.parse to convert the string values passed to the
		        // correct type. Ex. 'isLength:1' will make '1' actually a number
		        args = args.map((arg) => { return JSON.parse(arg); });

		        // We then merge two arrays, ending up with the value
		        // to pass first, then options, if any. ['valueFromInput', 5]
		        args = [component.state.value].concat(args);

		        // So the next line of code is actually:

		        if (!validator[validateMethod].apply(validator, args)) {
		          isValid = false;
		        }

	    	})

	    }

	    // Now we set the state of the input based on the validation
	    component.setState({
	      isValid: isValid,
	      
	      // We use the callback of setState to wait for the state
	      // change being propagated, then we validate the form itself
	    }, this.validateForm);
	    
	},

	validateForm(){

		// We set allIsValid to true and flip it if we find any
	    // invalid input components
	    let allIsValid = true;

	    // Now we run through the inputs registered and flip our state
	    // if we find an invalid input component
	    let inputs = this.inputs;
	    Object.keys(inputs).forEach(function (name) {
	      if (!inputs[name].state.isValid) {
	        allIsValid = false;
	      }
	    });

	    // And last, but not least, we set the valid state of the
	    // form itself
	    this.setState({
	      isValid: allIsValid
	    });
	},

	// All methods defined are bound to the component by React JS, so it is safe to use "this"
	// even though we did not bind it. We add the input component to our inputs map
	attachToForm(component) {
		this.inputs[component.props.name] = component;

		// We add the value from the component to our model, using the
	    // name of the component as the key. This ensures that we
	    // grab the initial value of the input
	    this.model[component.props.name] = component.state.value;

	    // We have to validate the input when it is attached to put the
	    // form in its correct state
	    this.validate(component);
	},

	// We want to remove the input component from the inputs map
	detachFromForm(component) {
		delete this.inputs[component.props.name];

		// We of course have to delete the model property
	    // if the component is removed
	    delete this.model[component.props.name];
	},

	// We need a method to update the model when submitting the form.
	// We go through the inputs and update the model
	updateModel() {
		console.log(this.inputs);
		Object.keys(this.inputs).forEach(function (name) {
		  this.model[name] = this.inputs[name].state.value;
		}.bind(this));
	},

	getModel(){
		this.updateModel();
		console.log(this.model);
		return this.model;
	},

	// We prevent the form from doing its native
	// behaviour, update the model and log out the value
	submit(event) {
		event.preventDefault();
		this.updateModel();
		console.log(this.model);
		this.validateForm();
	},

	recursiveCloneChildren(children){
		return React.Children.map(children, child => {
			if(!child._isReactElement) return child;

			let childProps = {}
			if(FormComponents.indexOf(child.type.displayName) !== -1){

				childProps = {
		        	attachToForm: this.attachToForm,
		        	detachFromForm: this.detachFromForm,
		        	validate: this.validate
			    }
			    // console.log(child)
			    // console.log(childProps)
			}

		    childProps.children = this.recursiveCloneChildren(child.props.children);
		    return React.cloneElement(child, childProps);
		})
	},

	renderChildren(children){

		return React.Children.map(children, (child) => {

			if(!child._isReactElement){
				return child;
			}

			// We do a simple check for "name" on the child, which indicates it is an input.
      		// Might consider doing a better check though
			if(FormComponents.indexOf(child.type.displayName) !== -1) {
				
				// We attach a method for the input to register itself to the form
	       		// We attach a method for the input to detach itself from the form
       			// We also attach a validate method to the props of the input so
		        // whenever the value is upated, the input will run this validate method

		        const props = {
		        	attachToForm: this.attachToForm,
		        	detachFromForm: this.detachFromForm,
		        	validate: this.validate
		        };

		        const clone = React.cloneElement(child, props)
		        console.log(clone)
		        // If the child has its own children, traverse through them also...
				// in the search for inputs
				if (clone.props.children) {
					this.renderChildren(clone.props.children);
				}
		        return clone;
			}

			// If the child has its own children, traverse through them also...
			// in the search for inputs
			if (child.props.children) {
				this.renderChildren(child.props.children);
			}

			return child;

		}.bind(this))

	},

	render(){
		return (
		  <div>
		    {this.recursiveCloneChildren(this.props.children)}
		  </div>
		);
	}
})

export default Form;