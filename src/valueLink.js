export default function makeValueLink(component, key){
	return {
	    value: component.state[key],
	    requestChange: function(newValue) {
	      let newState = {};
	      newState[key] = newValue;
	      component.setState(newState);
	    }
  	}
}