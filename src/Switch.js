'use strict';
import React from 'react'
import classnames from 'classnames'
import mdlUpgrade from './common/mdlUpgrade'
 
const baseClasses = {
  'mdl-switch': true,
  'mdl-js-switch': true
};

class Switch extends React.Component {

  static propTypes = {
    className: React.PropTypes.string
  }

  render(){
    const {
      children,
      className,
      inputId,
      ripple,
      label
    } = this.props;

    const classes = classnames(baseClasses, {
    	'mdl-js-ripple-effect': ripple
    }, className);

    return (
      <label {...this.props} className={classes} htmlFor={inputId}>
        <input type="checkbox" id={inputId} className="mdl-switch__input" />
        <span className="mdl-switch__label">{label}</span>
      </label>
    );
  }
}

export default mdlUpgrade(Switch);