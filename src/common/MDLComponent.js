import { Children, Component, findDOMNode } from 'react';
import mdl from 'devdream-material-design-lite'

export default class MDLComponent extends Component {
    componentDidMount() {
        mdl.upgradeElement(findDOMNode(this));
    }

    componentWillUnmount() {
        mdl.downgradeElements(findDOMNode(this));
    }

    render() {
        return Children.only(this.props.children);
    }
}