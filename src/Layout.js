'use strict';
import React from 'react'
import classnames from 'classnames'
import mdlUpgrade from './common/mdlUpgrade'
 
const baseClasses = {
  'mdl-layout': true,
  'mdl-js-layout': true
};

class Layout extends React.Component {

  static propTypes = {
    className: React.PropTypes.string,
    overlayDrawerButton: React.PropTypes.bool,
    fixedDrawer: React.PropTypes.bool,
    fixedHeader: React.PropTypes.bool,
    largeScreenOnly: React.PropTypes.bool
  }

  render(){
    const {
      children,
      className,
      overlayDrawerButton,
      fixedDrawer,
      fixedHeader,
      largeScreenOnly
    } = this.props;

    const classes = classnames(baseClasses, {
      'mdl-layout--overlay-drawer-button': overlayDrawerButton,
      'mdl-layout--fixed-drawer': fixedDrawer,
      'mdl-layout--fixed-header': fixedHeader,
      'mdl-layout--large-screen-only': largeScreenOnly
    }, className);

    return (
      <div ref='layout' {...this.props} className={classes}>
        {children}
      </div>
    );
  }
}

export default mdlUpgrade(Layout);