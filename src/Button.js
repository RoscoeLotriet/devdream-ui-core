'use strict';
import React from 'react'
import classnames from 'classnames'
import mdlUpgrade from './common/mdlUpgrade'
import MDLComponent from './common/MDLComponent'

const baseClasses = {
  'mdl-button': true,
  'mdl-js-button': true
};

class Button extends React.Component {

  static propTypes = {
    className: React.PropTypes.string,
    fab: React.PropTypes.bool,
    miniFab: React.PropTypes.bool,
    icon: React.PropTypes.bool,
    colored: React.PropTypes.bool,
    primary: React.PropTypes.bool,
    accent: React.PropTypes.bool,
    ripple: React.PropTypes.bool 
  }

  static defaultProps = {
    ripple: true
  }
 
  render(){
    const {
      children,
      className,
      raised,
      fab,
      miniFab,
      icon,
      colored,
      primary,
      accent,
      ripple
    } = this.props;

    const classes = classnames(baseClasses, {
      'mdl-button--raised': raised,
      'mdl-button--fab': fab,
      'mdl-button--mini-fab': miniFab,
      'mdl-button--icon': icon,
      'mdl-button--colored': colored,
      'mdl-button--primary': primary,
      'mdl-button--accent': accent,
      'mdl-js-ripple-effect': ripple
    }, className);

    return (
      <div {...this.props} className={classes}>
        {children}
      </div>
    );
  }
}

export default mdlUpgrade(Button);
