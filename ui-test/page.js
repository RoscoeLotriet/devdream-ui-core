import React, { Component } from 'react';
// component
import {
    Badge,
    Form,
    FormTextField, 
    FormSelect, 
    Button,
    DateField,
    DateTimeField,
    DefaultCard,
    DefaultLayout,
    Grid,
    TextField, 
    Cell,
    Spinner,
    Select,
    Switch,
    Checkbox,
    PasswordField,
    ProgressBar
} from '../lib/main.js';

export class Page extends Component {

    render() {
        let v = this.props.color

        const options = [{value:'MR', name: 'Mr'}, {value:'MS', name: 'Ms'}]
        return (
            <DefaultLayout overlayDrawerButton waterfall fixedHeader fixedDrawer pageTitle="UI Core Showcase" drawerTitle="UI Core Showcase">

                <DefaultCard label="My Card" supportingText="My Card Text" actions={<Button raised ripple accent>My Button</Button>}>
                    <Switch inputId="my-switch" ripple label="My Switch"/>
                    <Checkbox inputId="my-checkbox" ripple label="My Checkbox"/>
                    <TextField label="My TextField" error="No way Jose!!" floatingLabel/>
                    <PasswordField label="My PasswordField" floatingLabel/>
                    <Badge value="42">My Badge</Badge>
                    <ProgressBar indeterminate/>
                    <Spinner active singleColor/>
                    <DateField label="My Date" floatingLabel/>
                    <DateTimeField label = "My DateTime" floatingLabel/>
                    <Select label="Title" name="title" floatingLabel options={options} addBlank/>
                    <input type="file" accept="image/*" capture="camera"/>
                </DefaultCard>

                
                
            </DefaultLayout>
        	);
    }
}
