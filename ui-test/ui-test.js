import React from 'react';
import {Page} from './page';

require('./style.less')
require('../src/polyfill.css')
require('../src/polyfill.js')
React.render(<Page />, document.getElementById('root'));