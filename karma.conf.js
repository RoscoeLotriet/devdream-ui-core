module.exports = function(config) {
  config.set({

    /**
     * These are the files required to run the tests.
     *
     * The `Function.prototype.bind` polyfill is required by PhantomJS
     * because it uses an older version of JavaScript.
     */
    files: [
      './bin/utils/polyfill.js',
      './spec/*[sS]pec.js'
    ],

    //Code coverage
    reporters: ['progress', 'coverage'],

    /**
     * The actual tests are preprocessed by the karma-webpack plugin, so that
     * their source can be properly transpiled.
     */
    preprocessors: {
      './spec/*[sS]pec.js': ['webpack']
    },

    coverageReporter: {
      type : 'html',
      dir : 'coverage/'
    },

    /**
     * Start these browsers for testing. PhantomJS is a headless browser.
     * Available browsers: https://npmjs.org/browse/keyword/karma-launcher
     */
    browsers: ['PhantomJS'],

    /**
     * Use jasmine as the test framework
     */
    frameworks: ['jasmine'],

    /**
     * The configuration for the karma-webpack plugin.
     * This is very similar to the main webpack.local.config.js.
     */
    webpack: {
      module: {
        loaders: [
          {
            test: /\.js$/,
            exclude: /node_modules/,
            loader: 'babel-loader',
            query: {
              auxiliaryCommentBefore: 'istanbul ignore next'
            }
          },
          {
            test: /\.css$/,
            loader: 'style-loader!css-loader'
          },
          {
            test: require.resolve('devdream-material-design-lite'),
            loader: 'exports?componentHandler'
          }
        ],
        postLoaders: [ 
          {  
            test: /\.js$/,
            exclude: /(spec|node_modules)\//,
            loader: 'istanbul-instrumenter-loader'
          }
        ]
      },
      resolve: {
        extensions: ['', '.js']
      }
    },

    /**
     * Turn off verbose logging of webpack compilation.
     */
    webpackMiddleware: {
      noInfo: true
    },

    singleRun: true,

    /**
     * Array of plugins used
     */
    plugins: [
      require("karma-coverage"),
      require("karma-jasmine"),
      require("karma-webpack"),
      require("karma-phantomjs-launcher")
    ]

  });
};